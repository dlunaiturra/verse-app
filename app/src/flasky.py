#!/usr/bin/env python3
import os
from flask import Flask
from flask_restful import Api
from dotenv import load_dotenv, find_dotenv
from .config import Config
from .resources import resources
from .extensions import db

load_dotenv(find_dotenv())


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    api = Api(app, prefix=os.getenv('API_ENDPOINT'))
    configure_resources(api)
    configure_extensions(app)
    return app


def configure_resources(api):
    for res in resources:
        if hasattr(res, 'ENDPOINTS'):
            api.add_resource(res, *res.ENDPOINTS)
            print('\n# Resource: {}, Endpoints: {} \n'.format(res.__name__, " ".join(res.ENDPOINTS)))


def configure_extensions(app):
    db.init_app(app)
