from flask_restful import Resource, reqparse
from ..models import Operation, User


class OperationResource(Resource):
    ENDPOINTS = ['/operation']

    def post(self):
        data = self._parser()
        # Check if users exists.
        if User.get_by_id(data['user_id']) is not None and User.get_by_id(data['beneficiary_id']) is not None:
            # Transfers
            if data['operation_type'] in 'transfer':
                if Operation.get_balance_by_user_id(data['user_id']) >= float(data['amount']):
                    beneficiary_data = {'operation_type': 'transfer', 'amount': data['amount'],
                                        'user_id': data['beneficiary_id'],
                                        'beneficiary_id': data['user_id']}
                    data['amount'] = -data['amount']
                    oper_1 = Operation(**data)  # Owner register
                    oper_2 = Operation(**beneficiary_data)  # Beneficiary register
                    oper_1.save_to_db()
                    oper_2.save_to_db()
                    return {
                        'data': oper_1.as_dict(),
                        'status': 'success'
                    }
                else:
                    return {
                        'data': 'No enough credits.',
                        'status': 'error'
                    }
            # Deposits
            elif data['operation_type'] in 'deposit':
                data['beneficiary_id'] = data['user_id']
                op = Operation(**data)
                if op.save_to_db():
                    return {
                        'data': op.as_dict(),
                        'status': 'success'
                    }
        return {'data': 'Invalid data!', 'status': 'error'}

    @staticmethod
    def _parser():
        parser = reqparse.RequestParser()
        parser.add_argument('operation_type', type=str, required=True, help='Type: ["deposit", "transfer"]')
        parser.add_argument('concept', type=str)
        parser.add_argument('amount', type=float, required=True, help='Amount required!')
        parser.add_argument('user_id', type=int, required=True, help="Operation's owner")
        parser.add_argument('beneficiary_id', type=int, required=True, help='Type: deposit use user_id')
        return parser.parse_args()
