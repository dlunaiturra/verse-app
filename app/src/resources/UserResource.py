from flask_restful import Resource, reqparse
from flask import request
from ..models import User, Operation


class UserResource(Resource):
    ENDPOINTS = ['/user', '/user/<int:pk>']

    def get(self, pk=None):
        if pk is not None:
            try:
                user = User.get_by_id(pk).as_dict()
                # user['operations'] = [op.as_dict() for op in Operation.get_operations_by_user_id(user['id'])]
                user['balance'] = Operation.get_balance_by_user_id(user['id'])
                return {'data': user}
            except Exception as err:
                print(err)
        return {'data': "User not found."}

    def post(self):
        data = self._parser()
        check_username = User.get_user_by_username(data['username'])
        check_email = User.get_user_by_email(data['email'])

        if not check_username and not check_email:
            try:
                user = User(**data)
                if user.save_to_db():
                    return {'data': user.as_dict()}
            except Exception as err:
                print(err)
        return {'data': 'User already exists.'}

    def put(self, pk):
        data = request.get_json()
        user = User.get_by_id(pk)

        if user is not None:
            try:
                user.load_from_dict(**data)
                if user.save_to_db():
                    return {'data': user.as_dict()}
            except Exception as err:
                print(err)
        return {'data': "User not found."}

    @staticmethod
    def _parser():
        parser = reqparse.RequestParser()
        parser.add_argument('username', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('balance', type=float)
        return parser.parse_args()
