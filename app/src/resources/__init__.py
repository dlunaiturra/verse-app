from .UserResource import UserResource
from .OperationResource import OperationResource

resources = [UserResource, OperationResource]
