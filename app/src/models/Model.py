from decimal import Decimal
from datetime import datetime
from sqlalchemy import exc
from sqlalchemy_utils.types.choice import Choice

from ..extensions import db


class Model(object):
    __idname__ = 'id'

    def __init__(self, **kwargs):
        self.load_from_dict(**kwargs)

    def load_from_dict(self, **kwargs):
        for key, value in kwargs.items():
            self.__setattr(key.lower(), value)

    def __setattr(self, key, value):
        try:
            setattr(self, key, value)
        except Exception as err:
            pass

    @classmethod
    def create_tables(cls):
        if not db.engine.dialect.has_table(db.engine, cls.__tablename__):
            db.metadata.drop_all(db.engine, tables=[cls.__table__], checkfirst=True)
            db.create_all()
            return True

    @classmethod
    def get_all(cls):
        return cls.query.all()

    def as_dict(self):
        obj = {}
        for c in self.__table__.columns:
            if isinstance(getattr(self, c.name), datetime):
                obj[c.name] = getattr(self, c.name).isoformat()
            elif isinstance(getattr(self, c.name), Decimal):
                obj[c.name] = float(getattr(self, c.name))
            elif isinstance(getattr(self, c.name), Choice):
                obj[c.name] = str(getattr(self, c.name).value)
            elif isinstance(getattr(self, c.name), bool):
                obj[c.name] = 'Active' if getattr(self, c.name) else 'Inactive'
            else:
                obj[c.name] = str(getattr(self, c.name))
        return obj

    def save_to_db(self):
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except exc.SAWarning as err:
            print(err)
            return False

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
