from sqlalchemy import ForeignKey, func, Sequence
from sqlalchemy_utils.types.choice import ChoiceType

from ..extensions import db
from .Model import Model
from ..utils import current_time


class Operation(Model, db.Model):
    __tablename__ = 'operations'
    TYPES = [
        ('deposit', 'Deposit'),
        ('transfer', 'Transfer')
        # ('withdraw', 'Withdraw')  # Possible??
    ]

    id = db.Column(db.Integer, primary_key=True)
    operation_type = db.Column(ChoiceType(TYPES))
    concept = db.Column(db.String, nullable=True)
    amount = db.Column(db.Numeric(20, 4), default=0)
    user_id = db.Column(db.Integer, ForeignKey('users.id'))
    beneficiary_id = db.Column(db.Integer)
    created_at = db.Column(db.TIMESTAMP, nullable=False, default=current_time())
    owner = db.relationship('User', foreign_keys=user_id)

    def __str__(self):
        return '<Operation {}>'.format(self.concept)

    @classmethod
    def get_operations_by_user_id(cls, __user_id):
        return cls.query.filter_by(user_id=__user_id).all()

    @classmethod
    def get_balance_by_user_id(cls, __user_id):
        balance = cls.query.with_entities(func.sum(cls.amount).label('balance')).filter_by(user_id=__user_id).first()[0]
        if balance is not None:
            return float(balance)
        return 0
