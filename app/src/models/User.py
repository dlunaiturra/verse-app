from ..extensions import db
from ..utils import current_time
from .Model import Model
from .Operation import Operation


class User(Model, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    status = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.TIMESTAMP, nullable=False, default=current_time())
    deleted_at = db.Column(db.TIMESTAMP, nullable=True, default=None)
    operations = db.relationship('Operation', backref='user', lazy='dynamic')

    def __str__(self):
        return '<User {}>'.format(self.username)

    def get_operations(self):
        return Operation.query.filter_by(user_id=self.id).all()

    @classmethod
    def get_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def get_user_by_username(cls, _username):
        return cls.query.filter_by(username=_username).first()

    @classmethod
    def get_user_by_email(cls, _email):
        return cls.query.filter_by(email=_email).first()
