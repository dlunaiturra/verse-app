import os


class Config(object):
    SECRET_KEY = 'verse'

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_BINDS = ''

    DB_USER = os.getenv('DB_USER', 'postgres')
    DB_PASS = os.getenv('DB_PASSWORD', 'postgres')
    DB_SERVER = os.getenv('DB_SERVER', 'db')
    DB_PORT = os.getenv('DB_PORT', 5342) if os.getenv('DB_PORT') is not None else 5342
    DB_NAME = os.getenv('DB_NAME', 'verse')

    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@{}/{}".format(DB_USER, DB_PASS, DB_SERVER, DB_NAME)
    # "postgresql://verse@postgres/verse"
