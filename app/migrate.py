# from builtins import range
from src import create_app
from src.models import models, Operation, User


def run_migrate():
    app = create_app()
    with app.app_context():
        for model in models:
            if model.create_tables():
                print('Success: ({})'.format(model.__name__))

        try:
            for x in range(0, 5):
                data = {'username': 'user_{}'.format(x), 'email': 'dluna{}@mail.com'.format(x)}
                user = User(**data)
                user.save_to_db()
        except Exception as err:
            pass

        try:
            data_transfer = {'id': 1, 'operation_type': 'transfer', 'concept': 'Comida lunes', 'amount': -20,
                             'user_id': 1,
                             'beneficiary_id': 2}
            new_operation = Operation(**data_transfer)
            new_operation.save_to_db()

            data_deposit = {'id': 2, 'operation_type': 'deposit', 'concept': 'Ingreso dinero', 'amount': 100,
                            'user_id': 1,
                            'beneficiary_id': 1}
            new_deposit = Operation(**data_deposit)
            new_deposit.save_to_db()
        except Exception as err:
            pass


if __name__ == '__main__':
    run_migrate()
