import os
from src import create_app
from src.models import models
from migrate import run_migrate

DEBUG = True if os.getenv('DEBUG') == 'TRUE' else False

app = create_app()
if __name__ == "__main__":
    run_migrate()
    app.run(host=os.getenv('HOST'), debug=DEBUG, port=os.getenv('PORT'), use_debugger=True, threaded=True)
